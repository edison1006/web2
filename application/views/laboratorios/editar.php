<form class="" action="<?php echo site_url(); ?>/laboratorios/insertarLaboratorio" method="post" id="frm_nuevo_laboratorio">
	<div id="modalEditarLaboratorio" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
    	<!-- Modal content-->
    	<div class="modal-content">
      	<div class="modal-header">
        	<h4 class="modal-title">Nuevo Laboratorio</h4>
        	<button type="button" class="close"
        	style="color:white;"
        	data-dismiss="modal">&times;</button>
      	</div>
      	<div class="modal-body">
        	<div class="row">
          	<div class="col-md-6">
            	<label for="">NOMBRE:</label><br>
            	<input type="text" class="form-control" required
            	name="nombre_lab" id="nombre_lab" value=""> <br>
            	<label for="">CAPACIDAD ESTUDIANTES:</label><br>
            	<input type="number" class="form-control" required
            	name="capacidad_lab" id="capacidad_lab" value="">
          	</div>
          	<div class="col-md-6">
            	<label for="">DESCRIPCION:</label><br>
            	<textarea name="descripcion_lab"
            	id="descripcion_lab"
            	class="form-control" required
            	rows="8"></textarea>
          	</div>
        	</div>
      	</div>
      	<div class="modal-footer">
        	<button type="submit" name="button" class="btn btn-primary">
            	Guardar
        	</button>
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      	</div>
    	</div>
  	</div>
	</div>
</form>
