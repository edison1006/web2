<h1>LISTADO DE LABORATORIOS</h1>

<?php if ($laboratorios): ?>
  <table class="table">
  	<thead>
    	<tr>
      	<th>ID</th>
      	<th>NOMBRE</th>
        <th>DESCRIPCION</th>
        <th>CAPACIDAD</th>
      	<th>ACCIONES</th>
    	</tr>
  	</thead>
  	<tbody>
    	<?php foreach ($laboratorios as $lab): ?>
      	<tr>
        	<td><?php echo $lab->id_lab; ?></td>
        	<td><?php echo $lab->nombre_lab; ?></td>
          <td><?php echo $lab->descripcion_lab; ?></td>
          <td><?php echo $lab->capacidad_lab; ?></td>
        	<td>
            	Editar &nbsp;
              <a href="javascript:void(0);" class="btn-eliminar" data-id="<?php echo $lab->id_lab; ?>" style="color: red;"><i class="mdi mdi-close"></i>Eliminar
              </a>
        	</td>
      	</tr>
    	<?php endforeach; ?>
  	</tbody>
  </table>
<?php else: ?>
<h1>NO HAY DATOS</h1>
<?php endif; ?>
<br>

<script type="text/javascript">

    function abrirVentana(url) {
      window.open(url, "_blank", "width=800,height=700");
    }

    // Evento click del botón "Eliminar"
    $(".btn-eliminar").click(function() {
      var idLaboratorio = $(this).data("id");

      // Mostrar un mensaje de confirmación
      if (confirm("¿Estás seguro de eliminar este seminario?")) {
        // Realizar la eliminación del seminario mediante una petición AJAX
        $.ajax({
          url: "<?php echo site_url('laboratorios/eliminar'); ?>/" + idLaboratorio,
          type: "POST",
          success: function(data) {
            toastr.success("Laboratorio eliminado exitosamente");
            cargarLaboratorios();// Cargar la tabla de nuevo
          },
          error: function() {
            toastr.error("Error al eliminar el laboratorio");
          }
        });
      }
    });
</script>
