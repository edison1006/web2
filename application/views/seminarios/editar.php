<h1>EDITAR SEMINARIO</h1>
<form class="" id="frm_nuevo_seminario" action="<?php echo site_url(); ?>/seminarios/procesarActualizacion" method="post" enctype="multipart/form-data">
    <div class="row">
      <input type="hidden" name="id_eggg" id="id_eggg" value="<?php echo $seminarioEditar->id_eggg; ?>">
      <div class="col-md-4">
          <label for="">Nombre del Seminario:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del seminario"
          class="form-control"
          required
          name="nombre_eggg" value="<?php echo $seminarioEditar->nombre_eggg; ?>"
          id="nombre_eggg">
      </div>
      <div class="col-md-4">
          <label for="">Duracion del seminario:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          required
          name="duracion_eggg" value="<?php echo $seminarioEditar->duracion_eggg; ?>"
          id="duracion_eggg">
      </div>
      <div class="col-md-4">
        <label for="">Costo del seminario:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el costo del seminario"
        class="form-control"
        name="costo_eggg" value="<?php echo $seminarioEditar->costo_eggg; ?>"
        id="costo_eggg">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Archivo:</label>
          <br>
          <input type="file" name="contenido_eggg" id="contenido_eggg" value="<?php echo $seminarioEditar->contenido_eggg; ?>">
      </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/seminarios/listado"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_nuevo_seminario").validate({
    rules:{
      nombre_eggg:{
        required:true,
        minlength:3,
        maxlength:150,
        letras:true
      },
      duracion_eggg:{
        required:true,
        minlength:3,
        maxlength:10,
      },
      costo_eggg:{
        required:true,
        minlength:3,
        maxlength:10
      }
    },
    messages:{
      nombre_eggg:{
        required:"Por favor ingrese el nombre del seminario",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto ",
      },
      duracion_eggg:{
        required:"Por favor ingrese la duracion del seminario",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto"
      },
      costo_eggg:{
        required:"Por favor ingrese el costo del seminario",
        minlength:"El titulo debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto"
      }
    }
  },
  submitHandler: function(formulario) {
        var formData = new FormData(formulario);

        $.ajax({
          url: "<?php echo site_url('seminarios/procesarActualizacion'); ?>",
          data: formData,
          type: 'post',
          processData: false,
          contentType: false,
          success: function(data) {
            toastr.success("Seminario insertado exitosamente");
            $("#modalNuevoSeminario").modal("hide");
            $("#frm_nuevo_seminario")[0].reset(); // Reiniciar el formulario
            cargarSeminarios();
          }
        });
      }
  });
</script>
<script type="text/javascript">
  $("#contenido_eggg").fileinput({
    language:"es"
  });
</script>
