<h1 class="text-center">LISTADO DE SEMINARIOS</h1>

  <?php if ($seminarios_eggg): ?>
    <table class="table table-striped table-bordered table-hover" id="tbl_seminarios">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>DURACIÓN</th>
          <th>COSTO</th>
          <th>CONTENIDO DEL SEMINARIO</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($seminarios_eggg as $filaTemporal): ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_eggg ?>
            </td>
            <td>
              <?php echo $filaTemporal->nombre_eggg ?>
            </td>
            <td>
              <?php echo $filaTemporal->duracion_eggg?>
            </td>
            <td>
              <?php echo $filaTemporal->costo_eggg ?>
            </td>
            <td>
              <?php if ($filaTemporal->contenido_eggg): ?>
                <a href="<?php echo base_url('uploads/').$filaTemporal->contenido_eggg; ?>" target="_blank">
                  <?php echo $filaTemporal->contenido_eggg ?>
                </a>
              <?php else: ?>

              <?php endif; ?>
            </td>
            <td class="text-center">
              <a href="#" onclick="editarSeminario(<?php echo $filaTemporal->id_eggg; ?>);">
                Editar
              </a>
               &nbsp;
              <a href="javascript:void(0);" class="btn-eliminar" data-id="<?php echo $filaTemporal->id_eggg; ?>" style="color: red;"><i class="mdi mdi-close"></i>Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php else: ?>
    <h1>No hay seminarios</h1>
  <?php endif; ?>
</div>
<br>
<script type="text/javascript">

    function abrirVentana(url) {
      window.open(url, "_blank", "width=800,height=700");
    }

    // Evento click del botón "Eliminar"
    $(".btn-eliminar").click(function() {
      var idSeminario = $(this).data("id");

      // Mostrar un mensaje de confirmación
      if (confirm("¿Estás seguro de eliminar este seminario?")) {
        // Realizar la eliminación del seminario mediante una petición AJAX
        $.ajax({
          url: "<?php echo site_url('seminarios/eliminar'); ?>/" + idSeminario,
          type: "POST",
          success: function(data) {
            toastr.success("Seminario eliminado exitosamente");
            cargarSeminarios();// Cargar la tabla de nuevo
          },
          error: function() {
            toastr.error("Error al eliminar el seminario");
          }
        });
      }
    });
</script>
