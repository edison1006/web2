<h3>CRUD ASINCRONO DE SEMINARIOS</h3>
<button type="button"
  class="btn btn-success"
  onclick="cargarSeminarios()"
  name="button">
  Recargar
</button>

<div class="row">
  <div class="col-md-12" id="contenedor-seminarios">
  </div>
</div>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalNuevoSeminario">
  Agregar Seminario
</button>

<form class="" action="<?php echo site_url(); ?>/seminarios/insertarSeminario" method="post" id="frm_nuevo_seminario" enctype="multipart/form-data">
	<div id="modalNuevoSeminario" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
    	<!-- Modal content-->
    	<div class="modal-content">
      	<div class="modal-header">
        	<h4 class="modal-title">Nuevo Seminario</h4>
        	<button type="button" class="close"
        	style="color:white;"
        	data-dismiss="modal">&times;</button>
      	</div>
      	<div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                  <label for="">Nombre del Seminario:
                    <span class="obligatorio">(Obligatorio)</span>
                  </label>
                  <br>
                  <input type="text"
                  placeholder="Ingrese el nombre del seminario"
                  class="form-control"
                  required
                  name="nombre_eggg" value=""
                  id="nombre_eggg">
              </div>
              <div class="col-md-6">
                  <label for="">Duracion del seminario:
                    <span class="obligatorio">(Obligatorio)</span>
                  </label>
                  <br>
                  <input type="text"
                  placeholder="Ingrese el primer apellido"
                  class="form-control"
                  required
                  name="duracion_eggg" value=""
                  id="duracion_eggg">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-6">
                <label for="">Costo del seminario:</label>
                <br>
                <input type="text"
                placeholder="Ingrese el costo del seminario"
                class="form-control"
                name="costo_eggg" value=""
                id="costo_eggg">
              </div>
              <div class="col-md-6">
                  <label for="">Archivo:</label>
                  <br>
                  <input type="file" name="contenido_eggg" id="contenido_eggg">
              </div>
            </div>
      	<div class="modal-footer">
        	<button type="submit" name="button" class="btn btn-primary">
            	Guardar
        	</button>
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      	</div>
    	</div>
  	</div>
	</div>
</form>





<div id="modalActualizarSeminario" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Actualizar Seminario</h4>
        <button type="button" class="close"
        style="color:white;"
        data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-actualizar-seminario">

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#contenido_eggg").fileinput({
    language:"es"
  });
</script>

<script type="text/javascript">
  $("#frm_nuevo_seminario").validate({
    rules:{
      nombre_eggg:{
        required:true,
        minlength:3,
        maxlength:150,
        letras:true
      },
      duracion_eggg:{
        required:true,
        minlength:3,
        maxlength:10,
      },
      costo_eggg:{
        required:true,
        minlength:3,
        maxlength:10
      }
    },
    messages:{
      nombre_eggg:{
        required:"Por favor ingrese el nombre del seminario",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto ",
      },
      duracion_eggg:{
        required:"Por favor ingrese la duracion del seminario",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto"
      },
      costo_eggg:{
        required:"Por favor ingrese el costo del seminario",
        minlength:"El titulo debe tener al menos 3 caracteres",
        maxlength:"Valor incorrecto"
      }
	},
  submitHandler: function(formulario) {
        var formData = new FormData(formulario);

        $.ajax({
          url: "<?php echo site_url('seminarios/insertarSeminario'); ?>",
          data: formData,
          type: 'post',
          processData: false,
          contentType: false,
          success: function(data) {
            toastr.success("Seminario insertado exitosamente");
            $("#modalNuevoSeminario").modal("hide");
            $("#frm_nuevo_seminario")[0].reset(); // Reiniciar el formulario
            cargarSeminarios();
          }
        });
      }
  });
</script>
<script type="text/javascript">
function cargarSeminarios(){
      $("#contenedor-seminarios").load("<?php echo site_url('seminarios/listado'); ?>");
}
cargarSeminarios();
</script>


<script type="text/javascript">
      function editarSeminario(id){
          $("#contenedor-actualizar-seminario").load("<?php echo site_url('seminarios/editar'); ?>/"+id);
          $("#modalActualizarSeminario").appendTo("body");
          $("#modalActualizarSeminario").modal("show");
      }
</script>
