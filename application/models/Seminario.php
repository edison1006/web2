<?php
  class Seminario extends CI_Model
  {
	function __construct()
	{
  	parent::__construct();
	}

	function insertar($datos){
    	return $this->db
            	->insert("seminario_eggg",
            	$datos);
	}
	//Funcion para consultar seminarioes
	function obtenerTodos(){
   	$listadoSeminarios=
   	$this->db->get("seminario_eggg");
   	if($listadoSeminarios
      	->num_rows()>0){//Si hay datos
      	return $listadoSeminarios->result();
   	}else{//No hay datos
      	return false;
   	}
	}

	function borrar($id_eggg){
  	$this->db->where("id_eggg",$id_eggg);
    if ($this->db->delete("seminario_eggg")) {
      return true;
    }
      return true;
	}

	function obtenerPorId($id_eggg){
  	$this->db->where("id_eggg",$id_eggg);
  	$seminario=$this->db->get("seminario_eggg");
  	if($seminario->num_rows()>0){
    	return $seminario->row();
  	}
  	return false;
	}

	function actualizar($id_eggg,$datos){
  	$this->db->where("id_eggg",$id_eggg);
  	return $this->db->update('seminario_eggg',$datos);
	}

  }//Cierre de la clase

 ?>
