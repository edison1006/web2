<?php
  class Laboratorio extends CI_Model
  {
	function __construct()
	{
  	parent::__construct();
	}

	function insertar($datos){
    	return $this->db
            	->insert("laboratorio",
            	$datos);
	}
	//Funcion para consultar laboratorioes
	function obtenerTodos(){
   	$listadoLaboratorios=
   	$this->db->get("laboratorio");
   	if($listadoLaboratorios
      	->num_rows()>0){//Si hay datos
      	return $listadoLaboratorios->result();
   	}else{//No hay datos
      	return false;
   	}
	}

	function borrar($id_lab){
  	$this->db->where("id_lab",$id_lab);
  	return $this->db->delete("laboratorio");
	}
  
	function obtenerPorId($id_lab){
  	$this->db->where("id_lab",$id_lab);
  	$laboratorio=$this->db->get("laboratorio");
  	if($laboratorio->num_rows()>0){
    	return $laboratorio->row();
  	}
  	return false;
	}

	function actualizar($id_lab,$datos){
  	$this->db->where("id_lab",$id_lab);
  	return $this->db->update('laboratorio',$datos);
	}

  }//Cierre de la clase

 ?>
