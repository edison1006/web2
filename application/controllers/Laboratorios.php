<?php
	class Laboratorios extends CI_Controller
	{
  	function __construct()
  	{
     	parent::__construct();
     	//cargar modelo
     	$this->load->model('Laboratorio');
     	if(!$this->session->userdata("conectado")){
       	redirect("welcome/login");
     	}
  	}

  	public function index(){
    	$this->load->view('header');
    	$this->load->view('laboratorios/index');
    	$this->load->view('footer');
  	}

		public function insertarLaboratorio(){
	     	$datos=array(
	       	"nombre_lab"=>$this->input->post("nombre_lab"),
	       	"capacidad_lab"=>$this->input->post("capacidad_lab"),
	       	"descripcion_lab"=>$this->input->post("descripcion_lab")
	     	);
	     	if($this->Laboratorio->insertar($datos)){
	         	$resultado=array("estado"=>"ok");
	     	}else{
	         	$resultado=array("estado"=>"error");
	     	}
	     	echo json_encode($resultado);
	   	}

			public function listado()
			{
				$data["laboratorios"]=$this->Laboratorio->obtenerTodos();
				$this->load->view("laboratorios/listado",$data);
			}

			public function eliminar($id_lab)
          {
              $seminario = $this->Laboratorio->obtenerPorId($id_lab);
                  if ($this->Laboratorio->borrar($id_lab)) {
                      $resultado = array("estado" => "ok");
                  } else {
                      $resultado = array("estado" => "error");
                  }
              header('Content-Type: application/json');
              echo json_encode($resultado);
          }
	}//Cierre de la clase
