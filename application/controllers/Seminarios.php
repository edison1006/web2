<?php
class Seminarios extends CI_Controller
{

  function __construct()
  {
    parent:: __construct();
    //cargar modelo
    $this->load->model('Seminario');
  }

    public function index(){
    	$this->load->view('header');
    	$this->load->view('seminarios/index');
    	$this->load->view('footer');
  	}
    public function insertarSeminario(){
	     	$datos=array(
          "nombre_eggg"=>$this->input->post('nombre_eggg'),
          "duracion_eggg"=>$this->input->post('duracion_eggg'),
          "costo_eggg"=>$this->input->post('costo_eggg'),
	     	);
        $this->load->library("upload");
        $new_name = "contenido_seminarios_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 1024*5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("contenido_eggg")) {
          $dataSubida = $this->upload->data();
          $datos["contenido_eggg"] = $dataSubida['file_name'];
            }
	     	if($this->Seminario->insertar($datos)){
	         	$resultado=array("estado"=>"ok");
	     	}else{
	         	$resultado=array("estado"=>"error");
	     	}
        header('Content-Type: application/json');
	     	echo json_encode($resultado);
	   	}

      public function listado()
			{
				$data["seminarios_eggg"]=$this->Seminario->obtenerTodos();
				$this->load->view("seminarios/listado",$data);
			}

      public function eliminar($id_eggg)
          {
              $seminario = $this->Seminario->obtenerPorId($id_eggg);
              if ($seminario) {
                  $rutaArchivo = FCPATH . 'uploads/' . $seminario->contenido_eggg;
                  if (file_exists($rutaArchivo)) {
                      unlink($rutaArchivo); // Eliminar el archivo
                  }
                  if ($this->Seminario->borrar($id_eggg)) {
                      $resultado = array("estado" => "ok");
                  } else {
                      $resultado = array("estado" => "error");
                  }
              } else {
                  $resultado = array("estado" => "error");
              }
              header('Content-Type: application/json');
              echo json_encode($resultado);
          }


          public function editar($id){
            $data["seminarioEditar"]=$this->Seminario->obtenerPorId($id);
              $this->load->view("seminarios/editar",$data);
          }

          public function procesarActualizacion()
          {
            $datosEditados=array(
             "nombre_eggg"=>$this->input->post('nombre_eggg'),
             "duracion_eggg"=>$this->input->post('duracion_eggg'),
             "costo_eggg"=>$this->input->post('costo_eggg'),
          );
            $id_eggg=$this->input->post("id_eggg");
            if ($this->Seminario->actualizar($id_eggg,$datosEditados)) {
              $this->session->set_flashdata("confirmacion","Instructor actualizado exitosamente");
            }else {
              $this->session->set_flashdata("error","Error al editar intenten nuevamente");
            }
            redirect("seminarios/index");
          }
}
